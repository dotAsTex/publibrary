﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using PublicationsLibrary.Models;
using PublicationsLibrary.ORM;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace PublicationsLibrary.Controllers
{
    public class SearchController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            if (Request.Cookies["userId"] != null)
            {
                if (SQLCreate.ExecuteSelectQuery("SELECT * FROM users where id=" + Request.Cookies["userId"]).Rows.Count > 0)
                {
                    return View();

                }

            }
            return View("Login");
        }
        [HttpGet]
        public ActionResult JsonRecent()
        {
            
            List<ArticleModel> models = new List<ArticleModel>();
            models.AddRange(Converter.ConvertArticle(SQLCreate.SelectQuery("articles", ArticleModel.fields, "random()<0.01 LIMIT 10"), ArticleModel.fields2));
            return Json(new { articles = models, max = 5 });
        }
        [HttpGet]
        public ActionResult Json(string query)
        {
            List<ArticleModel> models = new List<ArticleModel>();
            Dictionary<string, int> areas = new Dictionary<string, int>();
            List<string> finalAreas = new List<string>();
            List<int> areasItems = new List<int>();
            List<int> areasIDs = new List<int>();
            models = ORM.Converter.ConvertArticle(SQLCreate.SelectQuery("articles", ArticleModel.fields, "TITLE like '%"+Request.Query["search_string"]+"%' limit 15 offset " + int.Parse(Request.Query["page"])*15), ArticleModel.fields2).ToList();
            foreach (var model in models)
            {
                try
                {
                    areas.Add(model.ResearchArea.Name, 1);
                    areasIDs.Add(model.ResearchArea.ID);
                }
                catch
                {
                    areas[model.ResearchArea.Name]++;
                }

            }
            foreach (var a in areas)
            {
                finalAreas.Add(a.Key);
                areasItems.Add(a.Value);
            }
            return Json(new { max_id = models.Count, articles = models, areas = finalAreas, areasItems = areasItems, areasIDs = areasIDs, areas_cnt = areas.Count });
        }
    }
}
