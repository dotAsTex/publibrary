﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using PublicationsLibrary.Models;
using PublicationsLibrary.ORM;

namespace PublicationsLibrary.Controllers
{
    public class ArticleController : Controller
    {

        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Update()
        {
            try
            {
                ArticleModel article = new ArticleModel();
                article.ID = int.Parse(Request.Form["id"]);
                article.FirstPage = int.Parse(Request.Form["firstPage"]);
                article.LastPage = int.Parse(Request.Form["lastPage"]);
                article.Publisher = new PublisherModel() { Name = Request.Form["publisher"] };
                article.ResearchArea = new ResearchAreaModel() { Name = Request.Form["researchArea"] };
                article.ShortDescription = Request.Form["shortDescription"];
                article.Link = Request.Form["link"];
                article.Title = Request.Form["title"];
                article.Venue = new VenueModel() { Name = Request.Form["publication"], Date = Request.Form["Date"] };
                article.Authors = new List<AuthorModel>();
                foreach (var a in Request.Form["authors"].Split(';'))
                {
                    article.Authors.Add(new AuthorModel() { Name = a });
                }
                article.Keywords = new List<KeywordModel>();
                foreach (var a in Request.Form["keywords"].Split(';'))
                    article.Keywords.Add(new KeywordModel() { Content = a });

                article.ResolveKeywords();
                article.ResolveVenue();
                article.ResolveResearchArea();
                article.ResolvePublisher();
                SQLCreate.UpdateObject("articles", article.GenerateUpdateQuery());
                foreach (var a in article.Authors)
                {
                    try
                    {
                        a.ID = SQLCreate.InsertObject("authors", a.GenerateInsertQuery());
                    }
                    catch { }
                }
                article.BindAuthors();
                article.BindKeywords();
                return Redirect("/Article/ViewArticle?id=" + article.ID);
            }
            catch (Exception ex) { return Json(new { error = ex.Message }); }
        }
        [HttpPost]
        public IActionResult Add()
        {
            try
            {
                ArticleModel article = new ArticleModel();
                article.FirstPage = int.Parse(Request.Form["firstPage"]);
                article.LastPage = int.Parse(Request.Form["lastPage"]);
                article.Publisher = new PublisherModel() { Name = Request.Form["publisher"] };
                article.ResearchArea = new ResearchAreaModel() { Name = Request.Form["researchArea"] };
                article.ShortDescription = Request.Form["shortDescription"];
                article.Link = Request.Form["link"];
                article.Title = Request.Form["title"];
                article.Venue = new VenueModel() { Name = Request.Form["publication"], Date = Request.Form["Date"] };
                article.Authors = new List<AuthorModel>();
                foreach (var a in Request.Form["authors"].Split(';'))
                {
                    article.Authors.Add(new AuthorModel() { Name = a });
                }
                article.Keywords = new List<KeywordModel>();
                foreach (var a in Request.Form["keywords"].Split(';'))
                    article.Keywords.Add(new KeywordModel() { Content = a });

                article.ResolveKeywords();
                article.ResolveVenue();
                article.ResolveResearchArea();
                article.ResolvePublisher();
                article.ID = SQLCreate.InsertObject("articles", article.GenerateInsertQuery());
                foreach (var a in article.Authors)
                {
                    a.ID = SQLCreate.InsertObject("authors", a.GenerateInsertQuery());
                }
                article.BindAuthors();
                article.BindKeywords();
                return Redirect("/Article/ViewArticle?id=" + article.ID);
            }
            catch (Exception ex) { return Json(new { error = ex.Message }); }
        }
        [HttpGet]
        public IActionResult CreateArticle()
        {
            if (Request.Cookies["userId"] != null)
            {
                if (SQLCreate.ExecuteSelectQuery("SELECT * FROM users where id=" + Request.Cookies["userId"]).Rows.Count > 0)
                {
                    return View();

                }
            }
            return Redirect("/Account/Login");

        }
        [HttpGet]
        public IActionResult ViewArticle()
        {
            if (Request.Cookies["userId"] != null)
            {
                if (SQLCreate.ExecuteSelectQuery("SELECT * FROM users where id=" + Request.Cookies["userId"]).Rows.Count > 0)
                {
                    return View(ORM.Converter.ConvertArticle(SQLCreate.SelectQuery("articles", ArticleModel.fields, "ID=" + Request.Query["id"]), ArticleModel.fields2).ToList()[0]);

                }
            }
            return Redirect("/Account/Login");


        }
        [HttpGet]
        public IActionResult EditArticle()
        {
            if (Request.Cookies["userId"] != null)
            {
                if (SQLCreate.ExecuteSelectQuery("SELECT * FROM users where id=" + Request.Cookies["userId"]).Rows.Count > 0)
                {
                    var model = ORM.Converter.ConvertArticle(SQLCreate.SelectQuery("articles", ArticleModel.fields, "ID=" + Request.Query["id"]), ArticleModel.fields2).ToList()[0];
                    string kWords = "";
                    string authors = "";
                    if (model.Keywords != null)
                        foreach (var kw in model.Keywords)
                        {
                            try
                            {
                                kWords += kw.Content + ";";
                            }
                            catch { }
                        }
                    if (model.Authors != null)
                        foreach (var author in model.Authors)
                        {
                            if(author!=null)
                            authors += author.Name + ";";
                        }
                    if (kWords.Length > 1)
                        ViewData["Keywords"] = kWords.Substring(0, kWords.Length - 1);
                    if (authors.Length > 1)
                        ViewData["Authors"] = authors.Substring(0, authors.Length - 1);
                    return View(model);
                }
            }
            return Redirect("/Account/Login");

        }
    }
}
