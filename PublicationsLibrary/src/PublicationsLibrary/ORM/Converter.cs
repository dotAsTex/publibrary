﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using PublicationsLibrary.Models;
using System.Diagnostics;

namespace PublicationsLibrary.ORM
{
    public class Converter
    {
        public static IEnumerable<ArticleModel> ConvertArticle(DataTable dataTable, IEnumerable<string> fields)
        {
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                ArticleModel model = new ArticleModel();
                foreach (var field in fields)
                {
                    PropertyInfo propertyInfo = typeof(ArticleModel).GetProperty(field);
                    if (field == "ResearchArea")
                    {
                        model.ResearchArea = ConvertResearchArea(SQLCreate.ExecuteSelectQuery("SELECT * FROM researchareas WHERE ID=" + dataTable.Rows[i][field]), new List<string>() { "Name", "ID" });
                    }
                    else
                    {
                        if (field == "Authors")
                        {
                            model.Authors = ConvertAuthors(SQLCreate.ExecuteSelectQuery("SELECT * FROM article_authors WHERE article_id=" + dataTable.Rows[i]["ID"]), new List<string>() { "Name", "ID" }).ToList();
                            if (model.Authors.Count > 20)
                            {
                                Debug.Print("ALERT!");
                            }
                        }
                        else
                        {
                            if (field == "Venue_id")
                            {
                                model.Venue = ConvertVenue(SQLCreate.ExecuteSelectQuery("SELECT * FROM venues WHERE ID=" + dataTable.Rows[i]["venue_id"]), new List<string>() { "Name", "ID", "ISSN", "Date" });
                            }
                            else
                            {
                                if (field == "Publisher")
                                {
                                    model.Publisher = ConvertPublisher(SQLCreate.ExecuteSelectQuery("SELECT * FROM publishers WHERE ID=" + dataTable.Rows[i]["publisher"]), new List<string>() { "ID", "Name" });
                                }
                                else
                                {
                                    if (field == "Keywords")
                                    {
                                        model.Keywords = ConvertKeywords(SQLCreate.ExecuteSelectQuery("SELECT * FROM article_keywords WHERE article_id=" + dataTable.Rows[i]["ID"]), new List<string>() { "ID", "Content" }).ToList();

                                    }
                                    else
                                    {
                                        object data = dataTable.Rows[i][field];
                                        propertyInfo.SetValue(model, Convert.ChangeType(data, propertyInfo.PropertyType), null);
                                    }
                                }
                            }
                        }
                    }
                }
                yield return model;

            }
        }
        public static VenueModel ConvertVenue(DataTable dataTable, IEnumerable<string> fields)
        {
            VenueModel model = new VenueModel();
            foreach (var field in fields)
            {
                PropertyInfo propertyInfo = model.GetType().GetProperty(field);
                object data = dataTable.Rows[0][field];
                propertyInfo.SetValue(model, Convert.ChangeType(data, propertyInfo.PropertyType), null);
            }
            return model;
        }
        public static PublisherModel ConvertPublisher(DataTable dataTable, IEnumerable<string> fields)
        {
            PublisherModel model = new PublisherModel();
            foreach (var field in fields)
            {
                PropertyInfo propertyInfo = model.GetType().GetProperty(field);
                object data = dataTable.Rows[0][field];
                propertyInfo.SetValue(model, Convert.ChangeType(data, propertyInfo.PropertyType), null);
            }
            return model;
        }
        public static IEnumerable<KeywordModel> ConvertKeywords(DataTable dataTable, IEnumerable<string> fields)
        {
            List<int> ids = new List<int>();
            string query = "SELECT * FROM KEYWORDS WHERE ";
            bool changed = false;
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                query += "ID=" + dataTable.Rows[i][1].ToString() + " OR ";
                changed = true;
            }
            if (!changed)
            {
                yield return null;
            }
            else
            {
                query = query.Substring(0, query.Length - 4);
                dataTable = SQLCreate.ExecuteSelectQuery(query);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    KeywordModel model = new KeywordModel();
                    foreach (var field in fields)
                    {
                        PropertyInfo propertyInfo = model.GetType().GetProperty(field);
                        object data = dataTable.Rows[i][field];
                        propertyInfo.SetValue(model, Convert.ChangeType(data, propertyInfo.PropertyType), null);
                    }
                    yield return model;
                }
            }
        }
        public static IEnumerable<AuthorModel> ConvertAuthors(DataTable dataTable, IEnumerable<string> fields)
        {
            List<int> ids = new List<int>();
            string query = "SELECT * FROM AUTHORS WHERE ";
            bool changed = false;
            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                query += "ID=" + dataTable.Rows[i][1].ToString() + " OR ";
                changed = true;
            }
            if (!changed)
            {
                yield return null;
            }
            else
            {
                query = query.Substring(0, query.Length - 4);
                dataTable = SQLCreate.ExecuteSelectQuery(query);
                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    AuthorModel model = new AuthorModel();
                    foreach (var field in fields)
                    {
                        PropertyInfo propertyInfo = model.GetType().GetProperty(field);
                        object data = dataTable.Rows[i][field];
                        propertyInfo.SetValue(model, Convert.ChangeType(data, propertyInfo.PropertyType), null);
                    }
                    yield return model;
                }
            }
        }
        public static ResearchAreaModel ConvertResearchArea(DataTable dataTable, IEnumerable<string> fields)
        {
            ResearchAreaModel model = new ResearchAreaModel();
            foreach (var field in fields)
            {
                PropertyInfo propertyInfo = model.GetType().GetProperty(field);
                object data = dataTable.Rows[0][field];
                propertyInfo.SetValue(model, Convert.ChangeType(data, propertyInfo.PropertyType), null);
            }
            return model;
        }
    }

}
