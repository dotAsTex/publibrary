﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace PublicationsLibrary.ORM
{
    public static class SQLCreate
    {
        static string SQLConnectionString = "Host=localhost;Username=postgres;Password=root;Database=PubLibrary";
        public static int InsertObject(string table, string command)
        {
            return ExecuteQueryToGetID("INSERT INTO " + table + " " + command);
        }
        public static void UpdateObject(string table, string command)
        {
            ExecuteNonQuery("Update " + table + " SET " + command);
        }
        public static DataTable SelectQuery(string table, IEnumerable<string> fields, string condition)
        {
            string param = "";
            foreach(var field in fields)
            {
                param += field + ",";
            }
            string query = "SELECT " + param.Substring(0, param.Length - 1) + " FROM " + table + " WHERE " + condition;
            return ExecuteSelectQuery(query);
        }
        public static DataTable ExecuteSelectQuery(string command)
        {
            using (var conn = new NpgsqlConnection(SQLConnectionString))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    NpgsqlDataAdapter da = new NpgsqlDataAdapter(command, conn);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    return dt;
                }
            }
        }

        public static int GetCurrVal(string field)
        {
            using (var conn = new NpgsqlConnection(SQLConnectionString))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = "select currval('"+field+"');";
                    return cmd.ExecuteReader().GetInt32(1);
                }
            }
        }
        public static int ExecuteQueryToGetID(string command)
        {
            using (var conn = new NpgsqlConnection(SQLConnectionString))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = command + " RETURNING ID;";
                    return (int)cmd.ExecuteScalar();
                }
            }
        }
        public static int ExecuteNonQuery(string command)
        {
            using (var conn = new NpgsqlConnection(SQLConnectionString))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = command;
                    return cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
