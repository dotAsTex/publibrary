﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublicationsLibrary.ORM.DBModel
{
    [System.AttributeUsage(System.AttributeTargets.Field)]
    public class PrimaryKey : System.Attribute
    {
    }

    [AttributeUsage(System.AttributeTargets.Field)]
    public class ForeignKey : Attribute
    {
        private string ForeignModel;
        private string ForeignPK;

        public ForeignKey(string fModel, string fPK)
        {
            ForeignModel = fModel;
            ForeignPK = fPK;
        }
    }

    [AttributeUsage(AttributeTargets.Field)]
    public class ManyToManyRelation : Attribute
    {
        private string ForeignModel;
        private string ForeignModelPK;

        public ManyToManyRelation(string fModel, string fModelPK)
        {
            ForeignModel = fModel;
            ForeignModelPK = fModelPK;
        }
    }
}
