﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublicationsLibrary.Models
{
    public class ResearchAreaModel : IComparable<ResearchAreaModel>
    {
        private int iD;
        private string name;

        public int ID
        {
            get
            {
                return iD;
            }

            set
            {
                iD = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string GenerateInsertSql()
        {
            return "(name) VALUES('" + Name + "')"; 
        }
        public int CompareTo(ResearchAreaModel other)
        {
            if (ID == other.ID)
                {
                    return 0;
                }
              return 1;
        }
        public override string ToString()
        {
            return Name;
        }
    }
}
