﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublicationsLibrary.Models
{
    public class UserModel
    {
        private int iD;
        private string name;
        private bool isAdmin;
        private string passHash;

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public int ID
        {
            get
            {
                return iD;
            }

            set
            {
                iD = value;
            }
        }

        public bool IsAdmin
        {
            get
            {
                return isAdmin;
            }

            set
            {
                isAdmin = value;
            }
        }

        public string PassHash
        {
            get
            {
                return passHash;
            }

            set
            {
                passHash = value;
            }
        }
    }
}
