﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using PublicationsLibrary.ORM;
using System.Diagnostics;

namespace PublicationsLibrary.Models
{
    public class ArticleModel
    {
        public static List<string> fields = new List<string>() { "ID", "Title", "FirstPage", "LastPage", "ShortDescription", "Link", "Venue_id", "ResearchArea", "Publisher" };
        public static List<string> fields2 = new List<string>() { "ID", "Title", "FirstPage", "LastPage", "ShortDescription", "Link", "Venue_id", "ResearchArea", "Publisher", "Authors", "Keywords" };

        private int iD;
        private string title;
        private int firstPage;
        private int lastPage;
        private string shortDescription;
        private string type;
        private string link;
        private List<AuthorModel> authors;
        private VenueModel venue;
        private PublisherModel publisher;
        private List<KeywordModel> keywords;
        private ResearchAreaModel researchArea;

        public int ID
        {
            get
            {
                return iD;
            }

            set
            {
                iD = value;
            }
        }

        public string Title
        {
            get
            {
                return title;
            }

            set
            {
                title = value;
            }
        }

        public int FirstPage
        {
            get
            {
                return firstPage;
            }

            set
            {
                firstPage = value;
            }
        }

        public int LastPage
        {
            get
            {
                return lastPage;
            }

            set
            {
                lastPage = value;
            }
        }

        public string ShortDescription
        {
            get
            {
                return shortDescription;
            }

            set
            {
                shortDescription = value;
            }
        }

        public string Type
        {
            get
            {
                return type;
            }

            set
            {
                type = value;
            }
        }

        public string Link
        {
            get
            {
                return link;
            }

            set
            {
                link = value;
            }
        }

        public List<AuthorModel> Authors
        {
            get
            {
                return authors;
            }

            set
            {
                authors = value;
            }
        }

        public VenueModel Venue
        {
            get
            {
                return venue;
            }

            set
            {
                venue = value;
            }
        }

        public PublisherModel Publisher
        {
            get
            {
                return publisher;
            }

            set
            {
                publisher = value;
            }
        }

        public List<KeywordModel> Keywords
        {
            get
            {
                return keywords;
            }

            set
            {
                keywords = value;
            }
        }

        public ResearchAreaModel ResearchArea
        {
            get
            {
                return researchArea;
            }

            set
            {
                researchArea = value;
            }
        }
        public string GenerateUpdateQuery()
        {
            string result = "";
            if (Title != null) { result += "Title='" + Title + "',"; }
            result += "FirstPage="+ FirstPage + ",";
            result += "LastPage=" +LastPage + ",";
            if (ShortDescription != null) { result += "ShortDescription='"+ ShortDescription + "',"; }
            if (Type != null) { result += "Type='" + Type + "',"; }
            if (Venue != null) { result += "venue_id ="+Venue.ID + ","; }
            if (Publisher != null) { result += "publisher="+Publisher.ID + ","; }
            if (ResearchArea != null) { result += "researcharea="+ResearchArea.ID + ","; }
            if (Link != null) { result += "link='" + Link + "',"; }
            result = result.Substring(0, result.Length - 1);
            return result + " WHERE ID=" + ID;
        }
        public string GenerateInsertQuery()
        {
            string result = "(";
            string values = "(";
            if (Title != null) { result += "Title,"; values += "'" + Title + "',"; }
            result += "FirstPage,"; values += FirstPage + ",";
            result += "LastPage,"; values += LastPage + ",";
            if (ShortDescription != null) { result += "ShortDescription,"; values += "'" + ShortDescription + "',"; }
            if (Type != null) { result += "Type,"; values += "'" + Type + "'"; }
            if (Venue != null) { result += "venue_id,"; values += Venue.ID + ","; }
            if (Publisher != null) { result += "publisher,"; values += Publisher.ID + ","; }
            if (ResearchArea != null) { result += "researcharea,"; values += ResearchArea.ID + ","; }
            if (Link != null) { result += "link,"; values += "'" + Link + "',"; }
            result = result.Substring(0, result.Length - 1) + ")";
            values = values.Substring(0, values.Length - 1) + ")";
            return result + " VALUES" + values;
        }
        public void ResolveVenue()
        {
            try
            {

                var dt = SQLCreate.ExecuteSelectQuery("SELECT ID FROM venues WHERE name='" + Venue.Name + "'");
                Venue.ID = int.Parse(dt.Rows[0][0].ToString());
            }
            catch (Exception ex)
            {
                Venue.ID = SQLCreate.InsertObject("venues", Venue.GenerateInsertSql());
                Debug.Print(ex.Message);
            }
        }
        public void ResolvePublisher()
        {
            try
            {
                var dt = SQLCreate.ExecuteSelectQuery("SELECT ID FROM publishers WHERE name='" + Publisher.Name + "'");
                Publisher.ID = int.Parse(dt.Rows[0][0].ToString());
            }
            catch (Exception ex)
            {
                Publisher.ID = SQLCreate.InsertObject("publishers", Publisher.GenerateInsertSql());

                Debug.Print(ex.Message);
            }

        }
        public void ResolveResearchArea()
        {
            try
            {
                var dt = SQLCreate.ExecuteSelectQuery("SELECT ID FROM researchareas WHERE name='" + ResearchArea.Name + "'");
                ResearchArea.ID = int.Parse(dt.Rows[0][0].ToString());

            }
            catch (Exception ex)
            {
                ResearchArea.ID = SQLCreate.InsertObject("researchareas", ResearchArea.GenerateInsertSql());
                Debug.Print(ex.Message);
            }

        }
        public void ResolveKeywords()
        {
            foreach (var keyword in Keywords)
            {
                try
                {
                    var dt = SQLCreate.ExecuteSelectQuery("SELECT ID FROM keywords WHERE content='" + keyword.Content + "'");
                    keyword.ID = int.Parse(dt.Rows[0][0].ToString());
                }
                catch
                {
                    keyword.ID = SQLCreate.InsertObject("keywords", keyword.GenerateInsertSql());
                }
            }

        }
        public void BindAuthors()
        {

            SQLCreate.ExecuteNonQuery("DELETE FROM article_authors where article_id=" + ID);
            foreach (var author in Authors)
            {
                try
                {
                    SQLCreate.ExecuteNonQuery("INSERT INTO article_authors(article_id,author_id) VALUES(" + ID + "," + author.ID + ")");

                }
                catch { }
            }
        }
        public void BindKeywords()
        {
            SQLCreate.ExecuteNonQuery("DELETE FROM article_keywords where article_id=" + ID);
            foreach (var keyword in Keywords)
            {
                try
                {
                    SQLCreate.ExecuteNonQuery("INSERT INTO article_keywords(article_id, keyword_id) VALUES(" + ID + "," + keyword.ID + ")");
                }
                catch { }
            }
        }
    }
}
