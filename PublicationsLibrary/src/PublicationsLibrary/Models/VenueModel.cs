﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublicationsLibrary.Models
{
    public class VenueModel
    {
        private int iD;
        private string name;
        private string iSSN;
        private string date;

        public int ID
        {
            get
            {
                return iD;
            }

            set
            {
                iD = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string ISSN
        {
            get
            {
                return iSSN;
            }

            set
            {
                iSSN = value;
            }
        }

        public string Date
        {
            get
            {
                return date;
            }

            set
            {
                date = value;
            }
        }

        public string GenerateInsertSql()
        {
            return "(name,date) VALUES ('" + Name + "','" + Date + "')";
        }
    }
}
