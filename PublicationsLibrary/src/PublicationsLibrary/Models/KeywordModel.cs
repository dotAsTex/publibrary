﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublicationsLibrary.Models
{
    public class KeywordModel
    {
        private int iD;
        private string content;

        public int ID
        {
            get
            {
                return iD;
            }

            set
            {
                iD = value;
            }
        }

        public string Content
        {
            get
            {
                return content;
            }

            set
            {
                content = value;
            }
        }

        public string GenerateInsertSql()
        {
            return "(content) VALUES('" + Content + "')";
        }
    }
}
