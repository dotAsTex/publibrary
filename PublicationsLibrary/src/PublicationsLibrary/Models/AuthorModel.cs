﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublicationsLibrary.Models
{
    public class AuthorModel
    {
        private int id;
        private string institution;
        private string name;

        public int ID
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Institution
        {
            get
            {
                return institution;
            }

            set
            {
                institution = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string GenerateInsertQuery()
        {
            string fields = "(name) VALUES('" + Name + "')";
            return fields; 
        }
    }
}
