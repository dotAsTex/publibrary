﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PublicationsLibrary.Models
{
    public class PublisherModel
    {
        private int iD;
        private string name;

        public int ID
        {
            get
            {
                return iD;
            }

            set
            {
                iD = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string GenerateInsertSql()
        {
            return "(name) VALUES('" + Name + "')";
        }
    }
}
