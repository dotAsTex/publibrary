﻿using IEEEParser.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace IEEEParser
{
    class Program
    {
        static CookieContainer cc = new CookieContainer();
        static void Main(string[] args)
        {
          //  InitConnect();
            Parse("data");
        }
        static void Parse(string input)
        {
            var dict = new Dictionary<string, object>();
            //{"queryText":"data","refinements":["4291944822"],"pageNumber":"2","rowsPerPage":"100","searchWithin":[],"searchField":"Search_All"}
            dict.Add("queryText", "data");
            dict.Add("refinements", new string[] { "4291944822" });
            dict.Add("pageNumber", "1");
            dict.Add("rowsPerPage", "10");

            string html = GetHttpResponse("http://ieeexplore.ieee.org/rest/search", dict);
            html = html.Replace("[::", "").Replace("::]", "");
            var deserializer = Newtonsoft.Json.JsonSerializer.Create();
            var ro = deserializer.Deserialize<RootObject>(new JsonTextReader(new StringReader(html)));
            while (ro.endRecord < ro.totalRecords)
            {
                ro = deserializer.Deserialize<RootObject>(new JsonTextReader(new StringReader(html)));
                List<Article> articles = new List<Article>();
                foreach (var article in ro.records)
                {
                    try {
                        var art = new Article();
                        art.Title = article.title;
                        art.StartPage = article.startPage;
                        art.Publisher = article.publisher;
                        art.Publication = new Venue() { Title = article.publicationTitle, Year = article.publicationYear, Volume = article.volume };
                        art.IEEELink = article.articleNumber;
                        art.EndPage = article.endPage;
                        art.Content = article.@abstract;
                        art.authors = new List<Author>();
                        foreach (var author in article.authors)
                        {
                            art.authors.Add(new Author() { Name = author.normalizedName, Id = author.id });
                        }
                        articles.Add(art);
                        AddArticleToDb(art);
                        Console.WriteLine(art.IEEELink);
                    }
                    catch (Exception ex)
                    {
                        Debug.Print(ex.Message);
                    }
                }
                //deserializer.Serialize(new StreamWriter(input + " page " + dict["pageNumber"] + ".ieee"), articles);
                dict["pageNumber"] = (int.Parse((string)dict["pageNumber"]) + 1).ToString();
                html = GetHttpResponse("http://ieeexplore.ieee.org/rest/search", dict);
                html = html.Replace("[::", "").Replace("::]", "");

            }


        }
        public static void AddArticleToDb(Article article)
        {
            string payload = string.Format("firstPage={0}&lastPage={1}&publisher={2}&researchArea={3}&shortDescription={4}&link={5}&title={6}&publication={7}&date={8}&authors=", article.StartPage,
                article.EndPage, article.Publisher, "network", 
                article.Content.Replace("[::","").Replace("::]",""),
                article.IEEELink = "http://ieeexplore.ieee.org/xpl/articleDetails.jsp?arnumber=" + article.IEEELink,
                article.Title.Replace("[::","").Replace("::]",""),
                article.Publication.Title,
                article.Publication.Year);
            foreach(var a in article.authors)
            {
                payload += a.Name + ";";
            }
            payload = payload.Substring(0, payload.Length - 1);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://localhost:37086/Article/Add");
            request.Method = "POST";
            //request.Headers.Add("Cookie", "__utma=1.1287855891.1443584795.1443584795.1443584795.1; __utmz=1.1443584795.1.1.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); unicaID=fGTvhKna0bb-ZZts0Ct; __gads=ID=0a584502b6295e39:T=1443584833:S=ALNI_MaQYaAFywazW2NqModMz2BlLNweCg; ipCheck=81.22.202.222; TS0197bd77=012f3506238ffcc196c527b3225f3b46e5247051c5c0acebf29b52020cfbd4d8a40fcb597432ae846b303c881ad1222c83c546dfde382b058afef8ec44f1e74dc2e035b6a9; JSESSIONID=rbZfW1ZZHgTDMPbpXMH73MQ21Bv7v0X4YsrjNhhhdLvdv5XhhrV5!-1151942432; __atuvc=0%7C40%2C0%7C41%2C0%7C42%2C15%7C43%2C20%7C44; __atuvs=563591a23b2fc711012; x67my_txid=7976105d-621f-4d1c-8e5d-7abbdb908059%7C0; x67my_snid=6c7b6796-9a1c-4dca-b3f6-74b97f5db633; WLSESSION=891445900.20480.0000; TS0180cae3=012f35062321cc918f8bacf627bb1f94ff252e8290ba505f9b9be43029b18fd2d865d7f118b9f9c46088230bef1d35898b9f37c84988031e6c6a732896d61c73d04ae44767; visitstart=07:50; WT_FPC=id=2088321c94242d480f71446220729646:lv=1446324654740:ss=1446322465925");
            //request.CookieContainer = cc;
            //string reqData = "{" + string.Format("\"queryText\":\"{0}\",\"refinements\":[\"4291944822\"],\"searchWithin\":[],\"searchField\":\"Search_All\", \"pageNumber\"=\"{1}\"", param["queryText"],param["pageNumber"]) + "}";
            request.ContentType = "application/x-www-form-urlencoded";
            byte[] data = Encoding.UTF8.GetBytes(payload);
            request.ContentLength = data.Length;
            request.GetRequestStream().Write(data, 0, data.Length);
            request.GetRequestStream().Close();
            var response = (HttpWebResponse)request.GetResponse();


        }
        public static void InitConnect()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://ieeexplore.ieee.org/Xplore/home.jsp?reload=true");
            request.Method = "GET";
            request.CookieContainer = cc;
            var response = (HttpWebResponse)request.GetResponse();
            cc.Add(response.Cookies);
            var dict = new Dictionary<string, object>();
            //{"queryText":"data","refinements":["4291944822"],"pageNumber":"2","rowsPerPage":"100","searchWithin":[],"searchField":"Search_All"}
            dict.Add("queryText", "data");
            dict.Add("refinements", new string[] { "4291944822" });
            dict.Add("pageNumber", "1");
            dict.Add("rowsPerPage", "10");
            string html = GetHttpResponse("http://ieeexplore.ieee.org/rest/search", dict);
        }
        public static string GetHttpResponse(string link, Dictionary<string,object> param)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(link);
            request.Method = "POST";
            request.Headers.Add("Cookie", "__utma=1.1287855891.1443584795.1443584795.1443584795.1; __utmz=1.1443584795.1.1.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); unicaID=fGTvhKna0bb-ZZts0Ct; __gads=ID=0a584502b6295e39:T=1443584833:S=ALNI_MaQYaAFywazW2NqModMz2BlLNweCg; ipCheck=81.22.202.222; TS0197bd77=012f3506238ffcc196c527b3225f3b46e5247051c5c0acebf29b52020cfbd4d8a40fcb597432ae846b303c881ad1222c83c546dfde382b058afef8ec44f1e74dc2e035b6a9; JSESSIONID=rbZfW1ZZHgTDMPbpXMH73MQ21Bv7v0X4YsrjNhhhdLvdv5XhhrV5!-1151942432; __atuvc=0%7C40%2C0%7C41%2C0%7C42%2C15%7C43%2C20%7C44; __atuvs=563591a23b2fc711012; x67my_txid=7976105d-621f-4d1c-8e5d-7abbdb908059%7C0; x67my_snid=6c7b6796-9a1c-4dca-b3f6-74b97f5db633; WLSESSION=891445900.20480.0000; TS0180cae3=012f35062321cc918f8bacf627bb1f94ff252e8290ba505f9b9be43029b18fd2d865d7f118b9f9c46088230bef1d35898b9f37c84988031e6c6a732896d61c73d04ae44767; visitstart=07:50; WT_FPC=id=2088321c94242d480f71446220729646:lv=1446324654740:ss=1446322465925");
            //request.CookieContainer = cc;
            //string reqData = "{" + string.Format("\"queryText\":\"{0}\",\"refinements\":[\"4291944822\"],\"searchWithin\":[],\"searchField\":\"Search_All\", \"pageNumber\"=\"{1}\"", param["queryText"],param["pageNumber"]) + "}";
            string reqData = "{\"queryText\":\"data\",\"refinements\":[\"4291944822\"],\"pageNumber\":\""+param["pageNumber"]+"\",\"rowsPerPage\":\"10\",\"searchWithin\":[],\"searchField\":\"Search_All\"}";
            request.ContentType = "application/json;charset=UTF-8";
            byte[] data = Encoding.UTF8.GetBytes(reqData);
            request.ContentLength = data.Length;
            request.GetRequestStream().Write(data, 0, data.Length);
            request.GetRequestStream().Close();

            var response = (HttpWebResponse)request.GetResponse();
            cc.Add(response.Cookies);
            return new StreamReader(response.GetResponseStream()).ReadToEnd();
        }
    }
    
    class Article
    {
        public string Title;
        public string Content;
        public string Publisher;
        public List<Author> authors;
        public string IEEELink;
        public Venue Publication;
        public string StartPage;
        public string EndPage;
    }
    class Venue
    {
        public string Volume;

        public string Year;

        public string Title;
    }
    class Author
    {
        public string Name;
        public string Id;
    }
}
