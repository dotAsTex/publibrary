﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEEEParser.Entities
{
    public class UserInfo
    {
        public bool institute { get; set; }
        public bool member { get; set; }
        public bool individual { get; set; }
        public bool guest { get; set; }
        public bool subscribedContent { get; set; }
        public bool fileCabinetContent { get; set; }
        public string ip { get; set; }
        public bool showPatentCitations { get; set; }
        public bool showGet802Link { get; set; }
    }

    public class Author
    {
        public string id { get; set; }
        public string preferredName { get; set; }
        public string normalizedName { get; set; }
        public object nativeName { get; set; }
    }

    public class AccessType
    {
        public string type { get; set; }
        public string message { get; set; }
    }

    public class Record
    {
        public List<Author> authors { get; set; }
        public int patentCitationCount { get; set; }
        public AccessType accessType { get; set; }
        public string publicationNumber { get; set; }
        public string articleNumber { get; set; }
        public int citationCount { get; set; }
        public string isNumber { get; set; }
        public string publicationTitle { get; set; }
        public string endPage { get; set; }
        public string startPage { get; set; }
        public string doi { get; set; }
        public bool rightslinkFlag { get; set; }
        public bool ieeeGet802 { get; set; }
        public bool showMultiMedia { get; set; }
        public bool showHtml { get; set; }
        public bool ephemera { get; set; }
        public string pdfSize { get; set; }
        public string publicationYear { get; set; }
        public string pdfLink { get; set; }
        public string rightsLink { get; set; }
        public string publicationLink { get; set; }
        public string contentType { get; set; }
        public string title { get; set; }
        public string publisher { get; set; }
        public bool redline { get; set; }
        public bool handleProduct { get; set; }
        public bool showCheckbox { get; set; }
        public string @abstract { get; set; }
        public bool course { get; set; }
        public string docIdentifier { get; set; }
        public string volume { get; set; }
    }

    public class Child
    {
        public string value { get; set; }
        public string reference { get; set; }
        public string type { get; set; }
        public string key { get; set; }
    }

    public class BreadCrumb
    {
        public string type { get; set; }
        public string key { get; set; }
        public List<Child> children { get; set; }
        public string value { get; set; }
    }

    public class Child2
    {
        public string id { get; set; }
        public string name { get; set; }
        public int numRecords { get; set; }
    }

    public class Facet
    {
        public string id { get; set; }
        public string name { get; set; }
        public int numRecords { get; set; }
        public List<Child2> children { get; set; }
    }

    public class RootObject
    {
        public UserInfo userInfo { get; set; }
        public List<Record> records { get; set; }
        public List<BreadCrumb> breadCrumbs { get; set; }
        public bool showStandardDictionary { get; set; }
        public string searchType { get; set; }
        public List<Facet> facets { get; set; }
        public int startRecord { get; set; }
        public int endRecord { get; set; }
        public int totalRecords { get; set; }
        public bool subscribedContentApplied { get; set; }
        public bool promoApplied { get; set; }
    }
}
